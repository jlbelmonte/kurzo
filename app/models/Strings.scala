package models

object Strings {
  val badBase = "Base currency needs to be supplied"
val badDate = "Date must be of the format  yyyy-MM-dd'T'HH:mm:ss"
  val basePlease = "Base currency needs to be supplied"
  val badMatchers ="the provided currencies must be present in this list"

val singleCurrency = Rates("USD", " yyyy-MM-dd'T'HH:mm:ss", Map("BGN"->1.3188F))
val multipleCurreny = Rates("USD", " yyyy-MM-dd'T'HH:mm:ss",
  Map("BGN"->1.3188F, "THB"->26.034F, "TRY"->3.0295F, "USD"->0.78604F, "ZAR"->11.105F, "EUR"->0.67431F))

}




