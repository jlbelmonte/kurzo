package models

import actors.ExchangeManager
import play.api.libs.json.{JsValue, Json}

object RatesJsonTransformer {
  def formatData(query: ExchangeManager.Query, data: Rates): (JsValue, Int) = {
    val ratesMap = data.rates

    query.to match {
      case Some(currencyTo) =>
        //guard for the default currency
        val base = ratesMap.get(query.base) match {
          case Some(x) => Some(x)
          case None if query.base == "USD" => Some(1F)
          case _ => Option.empty[Float]
        }

        val to =  if(currencyTo =="USD") {
          Some(1F)
        } else ratesMap.get(currencyTo)
        
        (base, to) match {
          case (Some(base), Some(to))=>
            val newValue: Float = if (query.base == "USD") {
              to
            } else {
              to / base
            }
            (Json.toJson(Rates(query.base, data.date, Map(currencyTo -> newValue))), 200)
          case _ =>
            val message = Strings.badMatchers
            val values = ratesMap.keySet
            (Json.toJson(CurrencyNotFound(message, values)), 400)
        }
      case None =>
        if (query.base == "USD") {
          (Json.toJson(data), 200)
        } else {
          ratesMap.get(query.base) match {
            case Some(base) =>
              val transformedValues = ratesMap.map { case (k, v)
              => (k, v / base) } + ("USD" -> 1 / base) - query.base
              //add default base based in current
              (Json.toJson(Rates(query.base, data.date, transformedValues )), 200)
            case _ =>
              val message = Strings.badBase
              val values = ratesMap.keySet
              (Json.toJson(CurrencyNotFound(message, values)), 400)
          }

        }
    }

  }
}
