package models

import java.text.SimpleDateFormat
import java.util.Date

import play.api.libs.json.Json


object DateUtils {
  //2016-05-01T14:34:46Z

  val kurzoDateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

  val fixerioDateFormat: SimpleDateFormat = new SimpleDateFormat("yyy-MM-dd")

  def toDate(strDate: String): Date = kurzoDateFormat.parse(strDate)

  def fromFixerDate(strDate: String): Date = fixerioDateFormat.parse(strDate)

  def toFixerDate(strDate: Date): String = fixerioDateFormat.format(strDate)

  def toString(date: Date): String = kurzoDateFormat.format(date)
}

case class NoBaseError(reason: String)

object NoBaseError {
  implicit val format = Json.format[NoBaseError]
}


// API responses
sealed trait RatesResponses

case class Rate(symbol: String, value: Float) extends RatesResponses

object Rate {
  implicit val rateFormat = Json.format[Rate]
}


case class Rates(base: String, date: String, rates: Map[String, Float]) extends RatesResponses

object Rates {
  implicit val format = Json.format[Rates]
}


// API Errors
sealed trait RatesError

case class CurrencyNotFound(message: String, values: Set[String]) extends RatesError

object CurrencyNotFound {
  implicit val format = Json.format[CurrencyNotFound]
}

case class FailedOp(message: String) extends RatesError

object FailedOp {
  implicit val format = Json.format[FailedOp]
}



