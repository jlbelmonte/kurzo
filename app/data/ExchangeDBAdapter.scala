package data

import java.sql.Timestamp
import java.util.Date
import javax.inject.{Inject, Singleton}

import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}



case class Exchange(rates: String, timestamp: Timestamp)

@Singleton
class ExchangeHistory @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import driver.api._

  private class ExchangeTable(tag: Tag) extends Table[Exchange](tag, "exchanges") {

    def rates = column[String]("rates")

    def timestamp = column[Timestamp]("ts", O.PrimaryKey)

    def * = (rates , timestamp) <> ((Exchange.apply _).tupled, Exchange.unapply)
  }

  private val exchanges = TableQuery[ExchangeTable]

  def insert(date: Date, rates: String): Future[Unit] = db.run {
    println(date)
    println(rates)
    exchanges += Exchange(rates, new Timestamp(date.getTime))
  }.map(_ => ())


  def findByKey(date: Date): Future[Boolean] = {
    val ts = new Timestamp(date.getTime)
    val query = exchanges
      .filter(e => (e.timestamp === ts))
      .exists
      .result
    db.run(query)
  }

  def getLatest(): Future[Option[Exchange]] = {
    val query = exchanges
      .sortBy(_.timestamp.desc)
      .take(1)
      .result
    db.run(query).map(_.headOption)
  }

  def getByDate(date: Date): Future[Option[Exchange]] =  {
    val ts = new Timestamp(date.getTime)
    val query = exchanges
      .filter(e => (e.timestamp <= ts))
      .sortBy(_.timestamp.desc)
      .take(1)
      .result
     db.run(query).map(_.headOption)
  }

  def all(): Future[Seq[Exchange]] = db.run {
    exchanges.result
  }

}
