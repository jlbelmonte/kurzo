package actors

import java.util.Date

import actors.ExchangeManager.UpdateCurrencies
import akka.actor.{Actor, Cancellable}
import data.{Exchange, ExchangeHistory}
import models._
import org.apache.http.client.utils.URIBuilder
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.{Configuration, Logger}
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}


class ExchangeManager(exchanges: ExchangeHistory,
                      dbHandler: DatabaseConfig[JdbcProfile],
                      ws: WSClient,
                      config: Configuration)
                     (implicit val ec: ExecutionContext) extends Actor {

  val url = config.getString("currency.url")
  val webhooks = config.getString("webhooks.url")
  val latestPath = "/latest"

  var cancellable: Option[Cancellable] = Option.empty[Cancellable]
  override def preStart(): Unit = {
    cancellable = Some(context.system.scheduler.schedule(2.seconds, 6.minutes, self, UpdateCurrencies))
  }


  override def receive = {
    case UpdateCurrencies => updateLatestCurrencies()

    case q: ExchangeManager.Query => getRates(q)

  }


  private def getRates(q: ExchangeManager.Query): Future[Any] = {
    Logger.info(s"q")
    val senderRef = sender
    val dbResult = exchanges.getByDate(q.date)

    dbResult.onSuccess {
      case Some(ex) =>
        Logger.info(s"dfrom db $ex")
        //we try to update as much as we can but the api might just do it one every 24h
        val dbTime = ex.timestamp.getTime
        val requestTime = q.date.getTime
        if ((requestTime - dbTime) < 24.hours.toMillis) {
          val dateString = DateUtils.toString(new Date(ex.timestamp.getTime))

          senderRef ! Right(Rates(q.base, dateString, Json.parse(ex.rates).as[Map[String, Float]]))
        } else {
          fetchApi(q).foreach(senderRef ! _)
        }
      case None =>
        Logger.info(s"db knows nothing")
        fetchApi(q).foreach(senderRef ! _)
    }

    dbResult.recover {
      case e: Throwable =>
        senderRef ! Left(FailedOp(e.getMessage))
    }
  }


  def updateLatestCurrencies(): Future[Unit] = {
    val builder = new URIBuilder(s"$url$latestPath")
    builder.addParameter("base", "USD")
    val latestRate = exchanges.getLatest()
    val eventualNewRate = fetchAndStore(builder.build().toString)
    UpdateHooksIfNeeded(latestRate, eventualNewRate)
  }


  private def UpdateHooksIfNeeded(latestRate: Future[Option[Exchange]], eventualNewRate: Future[Either[FailedOp, Rates]]) = {
    def updateHook(newRate: Rates): Unit = {
      Logger.info(s"updating $newRate")
      ws.url(webhooks).post(Json.toJson(newRate))
    }

    for {
      rate <- latestRate
      newRate <- eventualNewRate
    } yield {
      (rate, newRate) match {
        case (Some(oldRate), Right(newRate)) =>
          val oldMap = Json.parse(oldRate.rates).as[Map[String, Float]]
          if (newRate.rates != oldMap) {
            val updated = ExchangeManager.extractNewRates(newRate.rates, oldMap)
            Logger.info(s"$updated")
            updateHook(newRate.copy(rates = updated))
          }
        case (None, Right(newRate)) =>
          updateHook(newRate)
        case (a,b) => Logger.info(s"GOT $a, $b")
        
      }
    }
  }

  def fetchApi(query: ExchangeManager.Query): Future[Either[FailedOp, Rates]] = {
    val path = s"/${DateUtils.toFixerDate(query.date)}"
    val builder = new URIBuilder(s"$url$path")
    builder.addParameter("base", "USD")

    fetchAndStore(builder.build().toString)
  }

  def fetchAndStore(url: String): Future[Either[FailedOp, Rates]] = {
    val request = ws.url(url)
      .withHeaders("Accept" -> "application/json")
      .withRequestTimeout(10000.millis)
    Logger.info(s"querying $url")

    val response = request.get()
    response.recover {
      case e =>
        Logger.error("Error fetching from Fixer api", e)
        Left(FailedOp("can't communicate with backend API"))

    }
    response.map { wsResponse =>
      Logger.info(s"response looks like $wsResponse")
      Json.parse(wsResponse.body).asOpt[Rates] match {
        case Some(rates) =>
          Logger.info(s"body parses to $rates")
          exchanges.findByKey(DateUtils.fromFixerDate(rates.date)).onSuccess { case exists =>
            Logger.info(s"exists query retured $exists")
            if (!exists) {
              Logger.info(s"goin to insert ${rates.rates}")
              exchanges.insert(DateUtils.fromFixerDate(rates.date), Json.stringify(Json.toJson(rates.rates)))
            }
          }
          Logger.info(s"returning success $rates")
          Right(rates)
        case None =>
          Logger.error(s"Response body from fixer  ${wsResponse.body}")
          Left(FailedOp("Can't format backend response"))
      }
    }
  }
}


object ExchangeManager {

  trait RatesActorMessage

  case class Query(base: String, to: Option[String], date: Date) extends RatesActorMessage

  case object UpdateCurrencies

  def extractNewRates(newRates: Map[String, Float], oldRates: Map[String, Float]): Map[String, Float] = {
    newRates.foldLeft(Map.empty[String, Float]) { (acc, tuple) =>
      val (curr, value) = tuple
      if (oldRates.get(curr).exists(_ != value) || oldRates.get(curr).isEmpty) {
        acc + tuple
      } else {
        acc
      }
    }
  }
}


