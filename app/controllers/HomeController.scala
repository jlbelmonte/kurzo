
package controllers

import java.util.Date
import javax.inject._

import actors.ExchangeManager
import akka.actor.{ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout
import data.ExchangeHistory
import io.swagger.annotations.{Api, ApiParam}
import models._
import play.Configuration
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc._
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}


//@Api
@Singleton
@Api(value="/rates",
  description = "Let's you check currency exchange rates",
  produces = "application/json")
//having issues with
// play.sbt.PlayExceptions$CompilationException: Compilation error[classfile annotation arguments have to be supplied as named arguments]
// @ApiResponses(
//  new ApiResponse(code = 400, message = Strings.basePlease, response = classOf[NoBaseError]),
//  new ApiResponse(code = 400, message = Strings.badBase, response = classOf[NoBaseError]),
//  new ApiResponse(code = 400, message = Strings.badDate, response = classOf[NoBaseError]),
//  new ApiResponse (code = 200, message = Json.stringify(Json.toJson(Strings.singleCurrency)),response = classOf[Rates]),
//  new ApiResponse (code = 200, message = Json.stringify(Json.toJson(Strings.multipleCurreny)),response = classOf[Rates])
//)
class HomeController @Inject()(exchanges: ExchangeHistory,
                               system: ActorSystem,
                               dbConfigProvider: DatabaseConfigProvider,
                               ws: WSClient,
                               config: Configuration) extends Controller {

  implicit val dbConfig: DatabaseConfig[JdbcProfile] = dbConfigProvider.get[JdbcProfile]

  implicit val ec = system.dispatcher
  //sorry about this time out. Fixer usually has a good response time but some times can exceed a second¯\_(")_/¯
  implicit val timeOut: Timeout = 2.seconds

  val exchangeProps = Props(new ExchangeManager(exchanges, dbConfig, ws, config))
  val ratesManager = system.actorOf(exchangeProps)

  def getRates(@ApiParam(value = "base currency")  base: String,
               @ApiParam(value = "target currency") target: Option[String],
               @ApiParam(value = "date") timestamp: Option[String]) = Action.async { request =>

    val unsafeDate = Try {
      request.getQueryString("timestamp").fold(new Date())(DateUtils.toDate(_))
    }
    unsafeDate match {
      case Success(date) =>
        val query = ExchangeManager.Query(base, target, date)
        val eventualRates = ratesManager ? query
        eventualRates.mapTo[Either[RatesError, Rates]].map {
          case Left(f: FailedOp) => ServiceUnavailable(Json.toJson(f))
          case Right(data) => RatesJsonTransformer.formatData(query, data) match {
            case (ok, 200) => Ok(ok)
            case (bad, 400) => BadRequest(bad)
          }
        }

      case Failure(_) =>
        Future.successful(BadRequest(Json.toJson(NoBaseError(Strings.badDate))))

    }

  }
}
