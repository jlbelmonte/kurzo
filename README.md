# Kurzo
Esperanto word for, 
 Currency exchange
 
### Disclaimer
Seems that fixer has some bad data like for example
[http://api.fixer.io/2011-05-01](http://api.fixer.io/2011-05-01)
it will return a blob from April 29th 2011
    
    ```{
      "base": "EUR",
      "date": "2011-04-29",
      "rates": {
        "AUD": 1.356,
        "BGN": 1.9558,
        "BRL": 2.3464,
        "CAD": 1.4102,
        "CHF": 1.2867,
        "CNY": 9.6456,
        "CZK": 24.223,
        "DKK": 7.4576,
        "GBP": 0.8917,
        "HKD": 11.543,
        "HRK": 7.3615,
        "HUF": 264.5,
        "IDR": 12728,
        "ILS": 5.0359,
        "INR": 65.703,
        "JPY": 120.67,
        "KRW": 1588.6,
        "LTL": 3.4528,
        "LVL": 0.7093,
        "MXN": 17.119,
        "MYR": 4.4015,
        "NOK": 7.782,
        "NZD": 1.8414,
        "PHP": 63.475,
        "PLN": 3.9356,
        "RON": 4.078,
        "RUB": 40.646,
        "SEK": 8.914,
        "SGD": 1.8205,
        "THB": 44.387,
        "TRY": 2.258,
        "USD": 1.486,
        "ZAR": 9.7994
      }
    }```
    

### How to run it
clone the repository 
```
git clone UR
cd kurzo 
sbt run
```

* before running the code you need to create a database called kurso and table. Schema following
  
### DB schema
```CREATE DATA BASE kurso```

 
```
CREATE TABLE IF NOT EXISTS exchanges (
       rates VARCHAR(2048) NOT NULL,
       ts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
       PRIMARY KEY ts (ts)
    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;
```
The reasoning behind making the timestamp unique is:
This timestamp has precission up to the second and this is not a high speed trading application, so we will never get the same date. Also this index will give us a great lookup time.



### API 
Swagger module is active but I was having some issues with an unexpected exception  so responses are not added, you can see commented @anotations in the main controller
 
if you want to see the swagger defition start the app and browse to
[http://localhost:9000/swagger.json](http://localhost:9000/swagger.json)


### Example requests

- [http://localhost:9000/rates?base=USD](http://localhost:9000/rates?base=USD)
- [http://localhost:9000/rates?base=CAD](http://localhost:9000/rates?base=CAD)
- [http://localhost:9000/rates?base=USD&target=CAD](http://localhost:9000/rates?base=USD&target=CAD)
- [http://localhost:9000/rates?base=CAD&target=USD](http://localhost:9000/rates?base=USD&target=CAD)
- [http://localhost:9000/rates?timestamp=2016-11-10T14:35:46Z&base=USD](http://localhost:9000/rates?timestamp=2016-11-10T14:35:46Z&base=USD)
- [http://localhost:9000/rates?timestamp=2016-11-10T14:35:46Z&base=HRK](http://localhost:9000/rates?timestamp=2016-11-10T14:35:46Z&base=HRK)



### Webhook 
the webhook is a fire and forget because I don't have an app running so no errors should pop up in the logs

### DataStore
I decided to back it up with a MySql database to reduce de requests to the external API and maintain availability in the case the external API is down if we have data.