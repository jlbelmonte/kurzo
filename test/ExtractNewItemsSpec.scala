import actors.ExchangeManager
import org.scalatest.{MustMatchers, WordSpec}

class ExtractNewItemsSpec extends WordSpec with MustMatchers {
  "Map extraction " should {
    "do nothing if same" in {
      //this case will never happen in runtime but, when was the last time you got hurt by a test?
      val map = Map[String, Float](("FOO"->0.1F))
      ExchangeManager.extractNewRates(newRates =map ,oldRates = map) mustBe Map.empty[String, Float]
    }
    "return the missing one" in {
      //this case will never happen in runtime but, when was the last time you got hurt by a test?
      val map1 = Map[String, Float](("FOO"->0.1F))
      val map2 = Map[String, Float](("BAR"->0.1F))
      ExchangeManager.extractNewRates(newRates =map1 ,oldRates = map2) mustBe map1
    }

    "return only the new updated ones and missing" in {
      //this case will never happen in runtime but, when was the last time you got hurt by a test?
      val map1 = Map[String, Float](("FOO"->0.1F), ("BAR" -> 0.2F))
      val map2 = Map[String, Float](("BAR"->0.1F))
      ExchangeManager.extractNewRates(newRates =map1 ,oldRates = map2) mustBe map1
    }
    "return only the new updated ones" in {
      //this case will never happen in runtime but, when was the last time you got hurt by a test?
      val map1 = Map[String, Float](("FOO"->0.1F), ("BAR" -> 0.2F))
      val map2 = Map[String, Float](("FOO"->0.1F),("BAR"->0.1F))
      ExchangeManager.extractNewRates(newRates =map1 ,oldRates = map2) mustBe Map[String, Float]("BAR" -> 0.2F)
    }
  }
}
