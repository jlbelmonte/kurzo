import java.util.Date

import actors.ExchangeManager
import models._
import org.scalatest.{MustMatchers, WordSpec}
import play.api.libs.json._

class JsonTransFormerSpec extends WordSpec with MustMatchers {
 "RatesTransformer" should {
   val simpleRate = Rates("USD", "2017-11-03", Map("CAD" ->1.5F, "EUR" -> 2F))

   val eurBasedRates = Rates("EUR", "2017-11-03", Map("CAD" ->0.75F, "USD" -> 0.5F))

   val oneCurrentcy = Rates("USD", "2017-11-03", Map("EUR" -> 2F))

   val oneEURCurrentcy = Rates("EUR", "2017-11-03", Map("CAD" -> 0.75F))

   "return default base" in {
     val result = RatesJsonTransformer.formatData(ExchangeManager.Query("USD", None, new Date()), simpleRate)
     result mustBe((Json.toJson(simpleRate), 200))
   }
   "return default base only one currency" in {
     val result = RatesJsonTransformer.formatData(ExchangeManager.Query("USD", Some("EUR"), new Date()), simpleRate)
     result mustBe((Json.toJson(oneCurrentcy), 200))
   }

   "return default base only one currency in different base" in {
     val result = RatesJsonTransformer.formatData(ExchangeManager.Query("EUR", Some("CAD"), new Date()), simpleRate)
     result mustBe((Json.toJson(oneEURCurrentcy), 200))
   }

   "return list in another base and have USD" in {
     val result = RatesJsonTransformer.formatData(ExchangeManager.Query("EUR", None, new Date()), simpleRate)
     result mustBe((Json.toJson(eurBasedRates), 200))
   }

   "wrong base must fail" in {
     val result = RatesJsonTransformer.formatData(ExchangeManager.Query("FOO", None, new Date()), simpleRate)

     result mustBe (Json.toJson(CurrencyNotFound(Strings.badBase, Set("CAD", "EUR"))), 400)
   }
   "wrong currencies must fail" in {
     val result = RatesJsonTransformer.formatData(ExchangeManager.Query("FOO", Some("FOO"), new Date()), simpleRate)

     result mustBe (Json.toJson(CurrencyNotFound(Strings.badMatchers, Set("CAD", "EUR"))), 400)
   }

   "WWII Allies test EUR/USD" in {
     //{"base":"EUR","date":"2013-10-23T00:00:00","rates":{"USD":1.3751943111419678}}
     val eurBasedRates = Rates("USD", "2017-11-03", Map("EUR" -> 0.7271699905395508F))
     val flippedBase = Rates("EUR", "2017-11-03", Map("USD" -> 1.3751943111419678F))
     val result = RatesJsonTransformer.formatData(ExchangeManager.Query("EUR", Some("USD"), new Date()), simpleRate)
     result mustBe((Json.toJson( Rates("EUR", "2017-11-03", Map("USD" -> 0.5F))), 200))
   }
 }
}
