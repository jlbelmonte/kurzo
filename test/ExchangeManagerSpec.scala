import java.sql.{Date, Timestamp}

import actors.ExchangeManager
import actors.ExchangeManager.UpdateCurrencies
import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestActorRef, TestProbe}
import data.{Exchange, ExchangeHistory}
import models.{DateUtils, Rates}
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.MustMatchers
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.PlaySpec
import play.Configuration
import play.api.libs.json._
import play.api.libs.ws.WSClient
import play.api.mvc.{Action, Results}
import play.api.routing.sird._
import play.api.test.WsTestClient
import play.core.server.Server
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import scala.concurrent.Future
import scala.concurrent.duration._

class ExchangeManagerSpec extends PlaySpec with MustMatchers with MockitoSugar  {
  implicit val as = ActorSystem("managerTest")
  implicit val ec = as.dispatcher

  val simpleRate = Rates("EUR", "2017-11-03", Map("CAD" ->1.4830F))
  val date = DateUtils.fromFixerDate("2017-11-03")

  "ExchangeManager" should {
    "update and store if it doesn't exist" in {
      Server.withRouter() {
        case GET(p"/latest") => Action {
          Results.Ok(Json.toJson(simpleRate))
        }
        case POST(p"/webhooks") => Action {
          Results.Ok("")
        }
      } { implicit port =>
        WsTestClient.withClient { client =>
          val (dbHelper, manager) = testElements(client)
          when(dbHelper.getLatest()).thenReturn(Future.successful(None))
          when(dbHelper.findByKey(any(classOf[Date]))).thenReturn(Future.successful(false))
          when(
            dbHelper.insert(any(classOf[Date]), any(classOf[String]))
          ).thenReturn(Future.successful({}))

          manager.tell(UpdateCurrencies, TestProbe().ref)

          Thread.sleep(2000)
          verify(dbHelper, times(1)).getLatest()
          verify(dbHelper, times(1)).insert(any(classOf[Date]), any(classOf[String]))
        }
      }
    }

    "update but do not stor because exists" in {
      Server.withRouter() {
        case GET(p"/latest") => Action {
          Results.Ok(Json.toJson(simpleRate))
        }
        case POST(p"/webhooks") => Action {
          Results.Ok("")
        }
      } { implicit port =>
        WsTestClient.withClient { client =>
          val (dbHelper, manager) = testElements(client)
          when(dbHelper.getLatest()).thenReturn(Future.successful(None))
          when(dbHelper.findByKey(any(classOf[Date]))).thenReturn(Future.successful(true))
          when(
            dbHelper.insert(any(classOf[Date]), any(classOf[String]))
          ).thenReturn(Future.successful({}))

          manager.tell(UpdateCurrencies, TestProbe().ref)

          Thread.sleep(2000)
          verify(dbHelper, times(1)).getLatest()
          verify(dbHelper, never()).insert(any(classOf[Date]), any(classOf[String]))
        }
      }
    }

    "Query for  existing rate do not insert" in {
      Server.withRouter() {
        case GET(p"/latest") => Action {
          Results.Ok(Json.toJson(simpleRate))
        }
        case POST(p"/webhooks") => Action {
          Results.Ok("")
        }
      } { implicit port =>
        WsTestClient.withClient { client =>
          val (dbHelper, manager) = testElements(client)
          when(dbHelper.getByDate(any(classOf[Date])))
            .thenReturn(Future.successful(Some(Exchange("""{"CAD":0.1}""", new Timestamp((date.getTime))))))

          when(
            dbHelper.insert(any(classOf[Date]), any(classOf[String]))
          ).thenReturn(Future.successful({}))

          manager.tell(ExchangeManager.Query("USD", None, date), TestProbe().ref)

          Thread.sleep(2000)
          verify(dbHelper, never()).insert(any(classOf[Date]), any(classOf[String]))
        }
      }
    }

    "Query for  existing rate but is too old so insert a new one" in {
      Server.withRouter() {
        case GET(p"/2017-11-03") => Action {
          Results.Ok(Json.toJson(simpleRate))
        }
        case POST(p"/webhooks") => Action {
          Results.Ok("")
        }

      } { implicit port =>
        WsTestClient.withClient { client =>
          val (dbHelper, manager) = testElements(client)
          when(dbHelper.getByDate(any(classOf[Date])))
            .thenReturn(Future.successful(
              Some(Exchange("""{"CAD":0.1}""", new Timestamp((date.getTime - 26.hours.toMillis)))))
            )
          when(dbHelper.findByKey(any(classOf[Date]))).thenReturn(Future.successful(false))
          when(
            dbHelper.insert(any(classOf[Date]), any(classOf[String]))
          ).thenReturn(Future.successful({}))

          manager.tell(ExchangeManager.Query("USD", None, date), TestProbe().ref)

          Thread.sleep(2000)
          verify(dbHelper, times(1)).insert(any(classOf[Date]), any(classOf[String]))
        }
      }
    }

  }

  def testElements(ws: WSClient): (ExchangeHistory, TestActorRef[ExchangeManager]) = {
    import scala.collection.JavaConverters._
    val ddAccess = mock[ExchangeHistory]
    val db = mock[DatabaseConfig[JdbcProfile]]
    val config= new Configuration(Map[String, AnyRef](("currency.url", ""), ("webhooks.url", "")).asJava)

    val manager = TestActorRef[ExchangeManager](Props(new ExchangeManager(ddAccess, db, ws, config)))
    //cancel the auto update
    manager.underlyingActor.cancellable.foreach(_.cancel())
    (ddAccess, manager)

  }
}
