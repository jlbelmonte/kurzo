name := """kurzo"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  ws,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
  "org.mockito" % "mockito-all" % "1.9.5" % Test,
  "com.typesafe.akka" % "akka-testkit_2.11" % "2.4.14" % Test,
  "com.typesafe.play" %% "play-slick" % "2.1.0",
  "mysql" % "mysql-connector-java" % "5.1.12",
  "io.swagger" %% "swagger-play2" % "1.5.3"
)

